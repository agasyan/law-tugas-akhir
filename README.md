# LAW-Tugas Akhir

Agas Yanpratama - 1606918396

# How to run?

Prerequisites:
- bash to run `.sh`
- python 3
- Internet Connection to install required library
```bash
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```

Run all server:
```bash
python3 server1/manage.py migrate
python3 server1/.py collectstatic --noinput
python3 server1/manage.py runserver &
python3 server2/server2.py &
python3 server3/server3.py &
python3 server5/jam.py &
```

Kill all server:
`killall -9 python3`
