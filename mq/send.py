# RabbitMQ
import pika
import sys

# timeout
import signal
import time


# CONSTANTS
NPM = '1606918396'
USER = '0806444524'
PASS = '0806444524'
PORT_RABBITMQ = 5672
HOST_RABBITMQ = "152.118.148.95"
VIRTUAL_HOST = '/0806444524'

def publish_mq(msg, routing, exchange_type, logging = False):
    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=HOST_RABBITMQ, 
        port=PORT_RABBITMQ,
        virtual_host=VIRTUAL_HOST, 
        credentials=pika.PlainCredentials(USER, PASS)))
    channel = connection.channel()
    exchange_name = NPM + '_' + exchange_type
    channel.exchange_declare(exchange=exchange_name, exchange_type=exchange_type.lower())
    channel.basic_publish(exchange=exchange_name, routing_key=routing, body=str(msg))
    if logging: print(" [x] Sent %r:%r, routing:%r, exchange:%r" % (routing, msg, routing, exchange_name))
    connection.close()

def alarm_handler(signum, frame):
    raise TimeOutException()