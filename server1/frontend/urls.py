from django.urls import path
from django.conf.urls import include, url

from frontend import views


#url for app
urlpatterns = [
    #functions
    url(r'^$', views.index, name='index'),
    url(r'^result/$', views.result, name='result'),
]