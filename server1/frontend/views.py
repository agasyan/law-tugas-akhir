import uuid
import requests
import json
import sys
from django.shortcuts import render, redirect
import os

# If run with bash and want to get mq
sys.path.insert(1, os.getcwd())
from mq.send import publish_mq as publish 

# MQ Constants
mq = {
    'ex_topic' : '1606918396_TOPIC',
    'ex_direct' : '1606918396_DIRECT',
    'rkey_jam' : 'waktu_server',
    'rkey_s1_s2' : 's1tos2'
}

# Constants
SERVER_2 = "http://127.0.0.1:8002/"


# Create your views here.
def index(request):
    return render(request, 'index.html', {'mq':mq})

def result(request):
    if request.method == "POST":
        try:
            routing_id = str(uuid.uuid4()) 
            link_count = int(request.POST.get('linkCount'))
            links = []
            for i in range(1, (link_count+1)):
                link_obj = {}
                link_obj["id"] = i
                link_obj["link"] = request.POST.get('link-' + str(i))
                link_obj["download_progress"] = 0
                link_obj["path"] = ""
                link_obj["size"] = 0
                links.append(link_obj)
            # Send Post
            tmp_json = {}
            tmp_json['list-link'] = links
            tmp_json['routing-key'] = routing_id
            tmp_json['compress-percentage'] = 0

            # Publish to mq
            publish(json.dumps(tmp_json), mq['rkey_s1_s2'], 'DIRECT')

            return render(request, 'result.html', {'response': 'ok', 'routing': routing_id, 'links': links, 'link_count':link_count,'mq':mq})
        except Exception as e:
            msg = 'there was an error on links read on server 1, detail: "' + repr(e) +'"'
            status = 'Error Occured'
            print(msg)
            # if error return to index
            return render(request, 'result.html', {'status': status, 'response': 'Gagal dalam memproses request, silahkan coba kembali', 'mq':mq})
    else:
        # if method not post return to index
        return redirect(f'/')