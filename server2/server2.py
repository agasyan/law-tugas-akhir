# Import
import flask
import requests
import sys
import os
import json

from flask import request

# Threading
from threading import Thread

# wget
import wget

# If run with bash and want to get mq
sys.path.insert(1, os.getcwd())
from mq.send import publish_mq as publish 

# CONST
SERVER_3 = "http://127.0.0.1:8003/"


#!/usr/bin/env python
import pika
import sys

# CONSTANTS
NPM = '1606918396'
USER = '0806444524'
PASS = '0806444524'
PORT_RABBITMQ = 5672
HOST_RABBITMQ = '152.118.148.95'
VIRTUAL_HOST = '/0806444524'
tipe_ex = 'DIRECT'
rkey_s1_s2 = 's1tos2'

def main():
    # Create connection
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=HOST_RABBITMQ, 
        port=PORT_RABBITMQ,
        virtual_host=VIRTUAL_HOST, 
        credentials=pika.PlainCredentials(USER, PASS)))
    channel = connection.channel()

    exchange_name = NPM + '_' +tipe_ex
    channel.exchange_declare(exchange=exchange_name, exchange_type=tipe_ex.lower())
    result = channel.queue_declare('', exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=rkey_s1_s2)

    def callback(ch, method, properties, body):
        body_decode = body.decode('utf-8')
        body_json = json.loads(body_decode)
        arr_link = body_json['list-link']
        rkey = body_json['routing-key']
        thread_download_files(arr_link, rkey)

    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

    

def thread_download_files(arr_link, rkey):
    dowload_all_thread = Thread(target = download_all, args = (arr_link, rkey, ))
    dowload_all_thread.start()

def download_all(array_of_links_obj, routing_key):
    out_dir = "download_folder/" + routing_key
    tmp_file_id = 0
    next_num = 0 

    # Make directory
    os.makedirs("download_folder/" + routing_key)

    # For Downloading File
    def download_file(url):
        nonlocal out_dir, tmp_file_id
        filename = wget.download(url, out=out_dir, bar=bar_custom)
        array_of_links_obj[tmp_file_id]['path'] = filename
        array_of_links_obj[tmp_file_id]['size'] = os.path.getsize(filename)

    def bar_custom(current, total, width=80):
        # Update dict array
        now = int(current / total * 100)
        nonlocal next_num, tmp_file_id
        if now == next_num:
            next_num = next_num + 5
            array_of_links_obj[tmp_file_id]['download_progress'] = now
            send_progress_mq_dict(array_of_links_obj)

    def send_progress_mq_dict(array_links_dict):
        tmp_dct = {}
        tmp_dct['list-link'] = array_links_dict
        tmp_dct['compress-percentage'] = 0
        json_output = json.dumps(tmp_dct)
        publish(json_output, routing_key, 'TOPIC')

    for i in array_of_links_obj:
        tmp_file_id = (i['id']-1)
        next_num = 0
        download_file(i['link'])
    
    # Inform to server 3 Download is done
    out_dict = {}
    out_dict['list-link'] = array_of_links_obj
    out_dict['out-dir'] = out_dir 
    out_dict['compress-percentage'] = 0
    requests.post(SERVER_3, headers={
            'X-ROUTING-KEY': routing_key,
        }, json = json.dumps(out_dict))


if __name__ == '__main__':
    print("Starting Server 2 as Listener From Server 1")
    main()