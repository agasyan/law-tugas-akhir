# Import
import flask
import requests
import sys
import json


# Zip File Library
from pathlib import Path
import zipfile
import types
import os
from functools import partial
from os.path import basename

# For Hashing
import base64
import hashlib
import calendar
import datetime

from flask import request

# Threading
from threading import Thread

# If run with bash and want to get mq
sys.path.insert(1, os.getcwd())
from mq.send import publish_mq as publish 

# CONST
SERVER_4 = "http://127.0.0.1:8004"

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['POST'])
def compress():
    try:
        rkey = request.headers.get('X-Routing-Key')
        arr_link = json.loads(request.json)['list-link']
        out_dir = json.loads(request.json)['out-dir']
        thread_compress_files(arr_link, out_dir, rkey)
        response = {
            'response' : 'ok',
            'msg' : 'metadata_accepted'
        }
        return response
    except Exception as e:
        print(e)
        response = {
            'response' : 'error',
            'msg' : repr(e)
        }
        return response

def thread_compress_files(arr_link, out_dir, rkey):
    compress_thread = Thread(target = compress_folder, args = (arr_link, out_dir, rkey, ))
    compress_thread.start()

def compress_folder(array_of_links_obj, output_dir, routing_key):
    tmp_file_id = 0
    next_num = 0
    file_output = output_dir + "/" + "zipped_file" + ".zip"
    size_all_downloaded_resource = 0
    link_dir = []
    percentage_next = 0

    for i in array_of_links_obj:
        link_dir.append(i['path'])
        size_all_downloaded_resource += i['size']

    def compress_file(link_dir, out_file):
        progress.bytes = 0
        progress.obytes = 0
        progress.step = 0
        # progress.routing = routing_key
        with zipfile.ZipFile(out_file, 'w', compression=zipfile.ZIP_DEFLATED) as _zip:
            for file in link_dir:
                # Replace original write() with a wrapper to track progress
                _zip.fp.write = types.MethodType(partial(progress, os.path.getsize(file), _zip.fp.write), _zip.fp)
                _zip.write(file, basename(file))
        nonlocal routing_key
        # Hashing

        secret = "sECRetAgas"
        # Cwd
        curr_dir = os.getcwd()
        url = "/files/" + routing_key  + "/" + "zipped_file" + ".zip"
        full_path = curr_dir + "/" + "download_folder" + "/" + routing_key + "/" + routing_key + ".zip"
        exp_time = datetime.datetime.utcnow() + datetime.timedelta(minutes=60)
        expiry = calendar.timegm(exp_time.timetuple())

        secure_link = f"{secret}{url}{expiry}".encode('utf-8')
        hash = hashlib.md5(secure_link).digest()
        base64_hash = base64.urlsafe_b64encode(hash)
        str_hash = base64_hash.decode('utf-8').rstrip('=')

        dl_url = SERVER_4 + url + "?md5=" + str_hash + "&expires=" + str(expiry)

        # Send to mq
        tmp_dct = {}
        tmp_dct['list-link'] = array_of_links_obj
        tmp_dct['compress-percentage'] = 100
        tmp_dct['status'] = "done-compress"
        tmp_dct['download-url'] = dl_url
        json_output = json.dumps(tmp_dct)
        publish(json_output, routing_key, 'TOPIC')
        

    def progress(total_size, original_write, self, buf):
        progress.bytes += len(buf)
        progress.obytes += 1024 * 8  # Hardcoded in zipfile.write
        handle_percentage(progress.obytes)
        return original_write(buf)
    
    def handle_percentage(progress):
        nonlocal size_all_downloaded_resource, percentage_next, array_of_links_obj
        prog_now = int(100 * progress / size_all_downloaded_resource)
        if prog_now == percentage_next:
            if percentage_next == 100: percentage_next += 1000
            else: percentage_next += 5
            # print("{}% done ".format(prog_now))
            # Send to mq
            tmp_dct = {}
            tmp_dct['list-link'] = array_of_links_obj
            tmp_dct['compress-percentage'] = prog_now
            json_output = json.dumps(tmp_dct)
            publish(json_output, routing_key, 'TOPIC')
    
    # Compress
    compress_file(link_dir,file_output)
    


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8003)