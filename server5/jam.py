import time
from datetime import datetime

# get mq func
import sys
import os

# If run with bash and want to get mq
sys.path.insert(1, os.getcwd())
from mq.send import publish_mq as publish 

# timeout
import subprocess

# CONSTANTS
ROUTING_KEY = "waktu_server"

def out_time(waktu: datetime):
    if not isinstance(waktu, datetime):
        raise TypeError
    return waktu.strftime('%d/%m/%Y %H:%M:%S')

def publish_time():
    mulai = datetime.now()
    publish(out_time(mulai), ROUTING_KEY, 'TOPIC')
    while True:
        tmp = datetime.now()
        # Every Halfsecond up to publish
        if (tmp - mulai).microseconds >= 500000:
            mulai = tmp
            try:
                publish(out_time(mulai), ROUTING_KEY, 'TOPIC')
            except Exception as e:
                print(e)

if __name__ == '__main__':
    print("Clock Server Started")
    publish_time()
    
    
